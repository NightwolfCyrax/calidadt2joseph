﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly AppBibliotecaContext app;
        private readonly IBibliotecaRepository bibliotecaRepository;
        private readonly ISessionRepository sessionRepository;

        public BibliotecaController(AppBibliotecaContext app, IBibliotecaRepository bibliotecaRepository, ISessionRepository sessionRepository)
        {
            this.app = app;
            this.bibliotecaRepository = bibliotecaRepository;
            this.sessionRepository = sessionRepository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            bibliotecaRepository.SetHttpContext(HttpContext);
            return View(bibliotecaRepository.MostrarListaDeLibrosPorusuarioId());
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            bibliotecaRepository.SetHttpContext(HttpContext);
            bibliotecaRepository.GuardarEnLaBiblioteca(libro);

            //TempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            bibliotecaRepository.SetHttpContext(HttpContext);
            bibliotecaRepository.MarcarComoLeyendo(libroId);

            //TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user = LoggedUser();

            var libro = app.Bibliotecas
                .Where(o => o.LibroId == libroId && o.UsuarioId == user.Id)
                .FirstOrDefault();

            libro.Estado = ESTADO.TERMINADO;
            app.SaveChanges();

            //TempData["SuccessMessage"] = "Se marco como leyendo el libro";

            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            var user = app.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
