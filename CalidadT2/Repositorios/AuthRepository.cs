﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{

    public interface IAuthRepository {

        Usuario LogueoUsuarioPorUsernameYPassword(string username, string password);

    }



    public class AuthRepository : IAuthRepository
    {
        private readonly AppBibliotecaContext conexion;
        public AuthRepository(AppBibliotecaContext conexion)
        {
            this.conexion = conexion;
         }

        public Usuario LogueoUsuarioPorUsernameYPassword(string username, string password)
        {
          Usuario usuario = conexion .Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();

            if (usuario == null)
            {
                return null;
               
            }

            return usuario;

        }
    }
}
