﻿using CalidadT2.Constantes;
using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CalidadT2.Repositorios
{

    public interface IBibliotecaRepository {



        void SetHttpContext(HttpContext httpContext);
        List<Biblioteca> MostrarListaDeLibrosPorusuarioId();

        void MarcarComoLeyendo(int LibroId);
        void GuardarEnLaBiblioteca(int libro);


    }
    public class BibliotecaRepository : IBibliotecaRepository
    {
        private readonly AppBibliotecaContext conexion;
        private readonly ISessionRepository sessionRepository;
        private HttpContext httpContext;
   

        public BibliotecaRepository(AppBibliotecaContext conexion, ISessionRepository sessionRepository)
        {
            this.conexion = conexion;
            this.sessionRepository = sessionRepository;
          
        }

        public void SetHttpContext(HttpContext httpContext) {
            this.httpContext = httpContext;
        }
        public List<Biblioteca> MostrarListaDeLibrosPorusuarioId()
        {


            sessionRepository.SetHttpContext(httpContext);

            Usuario user = sessionRepository.UserLoggued();

            var model = conexion.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == user.Id)
                .ToList();

            return model;
        }

        public void GuardarEnLaBiblioteca(int libro)
        {
            sessionRepository.SetHttpContext(httpContext);
            Usuario user = sessionRepository.UserLoggued();

            var biblioteca = new Biblioteca
            {
                LibroId = libro,
                UsuarioId = user.Id,
                Estado = ESTADO.POR_LEER
            };

            conexion.Bibliotecas.Add(biblioteca);
            conexion.SaveChanges();

        }

        public void MarcarComoLeyendo(int libroId) {

            sessionRepository.SetHttpContext(httpContext);
            Usuario user = sessionRepository.UserLoggued();

            var libro = conexion.Bibliotecas
                .Where(o => o.LibroId == libroId && o.UsuarioId == user.Id)
                .FirstOrDefault();

            libro.Estado = ESTADO.LEYENDO;
            conexion.SaveChanges();
        }
    }
}
