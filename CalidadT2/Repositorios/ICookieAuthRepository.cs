﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{

    public interface ICookieAuthRepository {
        void SetHttpContext(HttpContext httpContext);
        public void Login(ClaimsPrincipal claim );
    }


    public class CookieAuthRepository : ICookieAuthRepository
    {
        private HttpContext httpContext;

        public void SetHttpContext(HttpContext httpContext) {

            this.httpContext = httpContext;
        }

        public void Login(ClaimsPrincipal claim)
        {
            httpContext.SignInAsync(claim);
        }
    }
}
