﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{

    public interface ILibroRepository {

        List<Libro> MuestraListaDeLibros();
        Libro DetalleLibroPorId(int LibroId);
    }

    public class LibroRepository : ILibroRepository
    {

        private readonly AppBibliotecaContext conexion;

        public LibroRepository(AppBibliotecaContext conexion)
        {
            this.conexion = conexion;
        }


        public List<Libro> MuestraListaDeLibros() {

            var model = conexion.Libros.Include(o => o.Autor).ToList();
            return model;

        }


        public Libro DetalleLibroPorId(int LibroId)
        {
            var model = conexion.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .Where(o => o.Id == LibroId)
                .FirstOrDefault();
            return model;

        }
    }
}
