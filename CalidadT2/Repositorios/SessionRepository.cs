﻿using CalidadT2.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{


    public interface ISessionRepository {

        void SetHttpContext(HttpContext httpContext);
        Usuario UserLoggued();
    }

    public class SessionRepository : ISessionRepository
    {
        private readonly AppBibliotecaContext conexion;
        private HttpContext httpContext;
        public SessionRepository(AppBibliotecaContext conexion)
        {
            this.conexion = conexion;
           
        }

        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

        public Usuario UserLoggued()
        {
            var claim = httpContext.User.Claims.FirstOrDefault();
            var user = conexion.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }
    }
}
