﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestControladores
{
    public class TestAuthController
    {



        [Test]

        public void CargaPaginaDeLogueo()
        {

            var repository = new Mock<IAuthRepository>();
            var authMock = new Mock<ICookieAuthRepository>();
            var controlador = new AuthController(repository.Object, authMock.Object);
            var vista = controlador.Login();
            Assert.IsInstanceOf<ViewResult>(vista);
        }


        [Test]
        public void UsuarioSeLogeaCorrectamente()
        {

            var repository = new Mock<IAuthRepository>();
            repository.Setup(o => o.LogueoUsuarioPorUsernameYPassword("admin", "admin")).Returns(new Usuario { });

            var authMock = new Mock<ICookieAuthRepository>();

            var controller = new AuthController(repository.Object, authMock.Object);
            var view = controller.Login("admin", "admin");
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }



    }

}
