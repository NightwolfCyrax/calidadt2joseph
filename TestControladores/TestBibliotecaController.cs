﻿using CalidadT2.Controllers;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestControladores
{
    public class TestBibliotecaController
    {


        [Test]

        public void MostrarTodosLosLibrosAñadidosALaLaBiblioteca() {

            var BibliotecaMock = new Mock<IBibliotecaRepository>();
            var SessionMock = new Mock<ISessionRepository>();

            var controlador = new BibliotecaController(null, BibliotecaMock.Object, SessionMock.Object);
            var vista = controlador.Index();
            Assert.IsInstanceOf<ViewResult>(vista);

        }


        [Test]

        public void AñadeUnLibroALaBibliotecaConELIdLibro8() {

            var BibliotecaMock = new Mock<IBibliotecaRepository>();
            var SessionMock = new Mock<ISessionRepository>();
            var controlador = new BibliotecaController(null, BibliotecaMock.Object, SessionMock.Object);
            var vista = controlador.Add(8);
            Assert.IsInstanceOf<RedirectToActionResult>(vista);
        }

        [Test]
        public void MarcarLibroComoLeyendoConElId1()
        {
            var BibliotecaMock = new Mock<IBibliotecaRepository>();
            var SessionMock = new Mock<ISessionRepository>();
            var controlador = new BibliotecaController(null, BibliotecaMock.Object, SessionMock.Object);
            var vista = controlador.MarcarComoLeyendo(1);
            Assert.IsInstanceOf<RedirectToActionResult>(vista);
        }

    }
}
