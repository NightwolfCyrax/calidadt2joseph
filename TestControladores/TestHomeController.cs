﻿using CalidadT2.Controllers;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestControladores
{
    public class TestHomeController
    {


        [Test]

        public void MuestraTodosLosLibrosDelHome() {

            var LibroMock = new Mock<ILibroRepository>();

            var controlador = new HomeController(LibroMock.Object);
            var vista = controlador.Index();
            Assert.IsInstanceOf<ViewResult>(vista);

        }
    }
}
