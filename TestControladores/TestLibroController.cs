﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestControladores
{
    public class TestLibroController
    {

        [Test]

        public void UsuarioPuedeVerDetalleDeLibro() {

            var LibroMock = new Mock<ILibroRepository>();
          

            LibroMock.Setup(o => o.DetalleLibroPorId(1)).Returns(new Libro());

            var controlador = new LibroController(LibroMock.Object, null);
            var vista = controlador.Details(1);
            Assert.IsInstanceOf<ViewResult>(vista);

        }


    }
}
